---
layout: markdown_page
title: "Product Vision - Distribution"
---

- TOC
{:toc}

GitLab is the engine that powers many companies' software businesses so it is
important to ensure it is as easy as possible to deploy, maintain, and stay up
to date.

Today we have a mature and easy to use Omnibus based build system, which is the
foundation for nearly all methods of deploying GitLab. It includes everything a
customer needs to run GitLab all in a single package, and is great for
installing on virtual machines or real hardware. We are committed to making our
package easier to work with, highly available, as well as offering automated
deployments on cloud providers like AWS.

We also want GitLab to be the best cloud native development tool, and offering a
great cloud native deployment is a key part of that. We are focused on offering
a flexible and scalable container based deployment on Kubernetes, by using
enterprise grade Helm charts.

The Helm charts are currently considered to be at the [Viable maturity level](https://about.gitlab.com/direction/maturity/). These epics will be used to define what it will take to get to the next maturity levels and track the work to be done:
[Viable to Complete](https://gitlab.com/groups/gitlab-org/-/epics/2260)
[Viable to Lovable](gitlab-org/charts/gitlab#1658)
